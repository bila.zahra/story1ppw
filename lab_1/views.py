from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Nabila Azzahra' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2001,4, 21) #TODO Implement this, format (Year, Month, Date)
npm = 1806205174 # TODO Implement this
angkatan = 2018
hobi = 'menabung dan menghamburkan uang'
desc = 'saya orang yang sangat ambis menabung dan langsung menghamburkan uang dalam beberapa menit'
kuliah = 'Universitas Indonesia'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm,
    'tahun' : angkatan, 'hobi': hobi, 'desc' : desc, 'kuliah' : kuliah}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
